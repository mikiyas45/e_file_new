// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  BASEURL: 'http://localhost:5013/tax/save',
  //MOCKURL: 'http://172.22.21.16:5014/withholding/save'
};

