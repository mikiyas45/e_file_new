import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionForPrivateComponent } from './pension-for-private.component';

describe('PensionForPrivateComponent', () => {
  let component: PensionForPrivateComponent;
  let fixture: ComponentFixture<PensionForPrivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PensionForPrivateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionForPrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
