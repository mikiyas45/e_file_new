import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploymentInTaxDeclarationComponent } from './employment-in-tax-declaration.component';

describe('EmploymentInTaxDeclarationComponent', () => {
  let component: EmploymentInTaxDeclarationComponent;
  let fixture: ComponentFixture<EmploymentInTaxDeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploymentInTaxDeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploymentInTaxDeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
