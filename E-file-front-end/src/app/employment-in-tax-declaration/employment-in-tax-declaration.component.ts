import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employment-in-tax-declaration',
  templateUrl: './employment-in-tax-declaration.component.html',
  styleUrls: ['./employment-in-tax-declaration.component.scss']
})
export class EmploymentInTaxDeclarationComponent implements OnInit {

  isDisable=true;

  constructor() { }

  ngOnInit() {
  }

    triggerSomeEvent() {
     this.isDisable=!this.isDisable;
  }

}
