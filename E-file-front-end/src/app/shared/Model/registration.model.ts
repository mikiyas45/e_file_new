export class RegistrationModel {
  private id: string;
  private first_name: string;
  private last_Name: string;
  private tin_Number: number;
  private vat_Number: number;

  public getId(): string {
    return this.id;
  }

  public setId(value: string) {
    this.id = value;
  }

  public getFirstName(): string {
    return this.first_name;
  }

  public setFirstName(value: string) {
    this.first_name = value;
  }

  public getLastName(): string {
    return this.last_Name;
  }

  public setLastName(value: string) {
    this.last_Name = value;
  }

  public getTinNumber(): number {
    return this.tin_Number;
  }

  public setTinNumber(value: number) {
    this.tin_Number = value;
  }

  public getVatNumber(): number {
    return this.vat_Number;
  }

  public setVatNumber(value: number) {
    this.vat_Number = value;
  }
}