export class CostSharingModel{
  private employees_tin_number : number;
  private first_name : string;
  private middile_name : string;
  private last_name : string;
  private Number_of_workers_paid_for_this_month : number;
  private The_gross_taxable_income_for_the_month : number;
  private Cost_sharing_costs : number;
}