export class PensionModel{
  private tin_No : string;
  private payment_period : Date;
  private Number_of_paid_workers_for_this_month : number;
  private Gross_wage_of_workers_per_month : number;
  private Total_Staff_Pension_Amount : number;
  private Total_Employer_Pension_Amount : number;
  private Total_Pension_Amount: number; 
  private Employee_Tin_Number : number;
  private first_name : string;
  private middle_name : string;
  private last_name : string;
}