export class vatDeclarationModel{
  private preparedBy:string;
  private valueOfTaxableGoodsAndSupplies:Number;
  private valueOfZeroRatedGoods:Number;
  private vlueOfExemptSupplies:Number;
  private valueOfRemittedSupplies:Number;
  private totalSupplies:Number;
  private vatOnTaxableGoodsAndSupplies:Number;
  private vatChargeForThisPeriod:Number;
  private valueOfLocalPurchasesInput:Number;
  private valueOfImportedInputs:Number;
  private valueOfGeneralExpenseInputs:Number;
  private valueOfPurchasesWithNoVat:Number;
  private valueOfTotalInputs:Number;
  private valueOfTaxableInputs:Number;
  private netVatDueForMonth:Number;
  private vatCreditForMonth:Number;
  private otherCreditsForMonth:Number;
  private oreditCarriedForwardFromPreviousMonth:Number;
  private amountToBePaid: Number;
  private creditAvailableForcarryForward:Number;
  private vatOnLocalPurchaseInput:Number;
  private vatOnInportedInputs:Number;
  private vatOnGeneralExpenseInputs:Number;
  private TotalInputTaxCredit:Number;
  private checkedBy:String;
  private is_active:{type:Boolean,'default':false}
}