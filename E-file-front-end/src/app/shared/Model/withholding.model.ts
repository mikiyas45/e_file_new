export class WithholdingModel{
  private witholder_tin : string;
  private taxable_two_percent : number;
  private number : number;
  private date : Date;
}