import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Language } from '../../Language';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LanguageEventManagerService {

    private selectedLanguage: BehaviorSubject<Language> = new BehaviorSubject<Language>(null);
    public selectedLanguageEmitter = this.selectedLanguage.asObservable();

    constructor() {}

    public showLanguage(language: Language) {
        this.selectedLanguage.next(language);
    }
}


