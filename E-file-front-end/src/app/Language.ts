export class Language {
    private code: string;
    private label: string;

    public static fromObject(obj): Language {
        const lang: Language = new Language();
        Object.assign(lang, obj);
        return lang;
    }

    public getCode(): string {
        return this.code;
    }
    public setCode(value: string) {
        this.code = value;
    }
    public getLabel(): string {
        return this.label;
    }
    public setLabel(value: string) {
        this.label = value;
    }

}
