import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TablesComponent } from './tables/tables.component';
import { TypographyComponent } from './typography/typography.component';
//import { IconsComponent } from './icons/icons.component';
import { AlertComponent } from './alerts/alerts.component';
import { AccordionsComponent } from './accordions/accordions.component';
import { BadgesComponent } from './badges/badges.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { PaginationComponent } from './pagination/pagination.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { CarouselComponent } from './carousel/carousel.component';
import { TabsComponent } from './tabs/tabs.component';
import { VATREGISTRATIONComponent } from './vat-registration/vat-registration.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
//import { RegistrationService } from './register/registration.service';
import { WithholdingComponent } from './withholding/withholding.component';
import { PensionForPrivateComponent } from 
'./pension-for-private/pension-for-private.component';
//import { environment } from '../environments/environment';
import { PensionForCivilComponent } from './pension-for-civil/pension-for-civil.component';

import { VatNewComponent } from './vat-new/vat-new.component';
import { MessageComponent } from './message/message.component';
import { AlertService } from '../app/alerts/alert.service'; 
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { WithholdingNewComponent } from './withholding-new/withholding-new/withholding-new.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {LanguageEventManagerService} from './shared/service/language-event-manager.service';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { LoginComponent } from './auth/Login/login.component';
import { signUpComponent } from './auth/signUp/signUp.component';
import { PensionComponent} from './pension/pension.component';
import { CostSharingComponent} from './cost-sharing/cost-sharing.component';
import { EmploymentInTaxDeclarationComponent } from './employment-in-tax-declaration/employment-in-tax-declaration.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    FormsComponent,
    ButtonsComponent,
    TablesComponent,
    TypographyComponent,
    PensionComponent,
   // IconsComponent,
    CostSharingComponent,
    LoginComponent,
    signUpComponent,
    AlertComponent,
    AccordionsComponent,
    BadgesComponent,
    ProgressbarComponent,
    BreadcrumbsComponent,
    PaginationComponent,
    DropdownComponent,
    TooltipsComponent,
    CarouselComponent,
    TabsComponent,
    VATREGISTRATIONComponent,
    RegisterComponent,
    WithholdingComponent,
    PensionForPrivateComponent,
    PensionForCivilComponent,
    VatNewComponent,
    MessageComponent,
    WithholdingNewComponent,
    EmploymentInTaxDeclarationComponent
    
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    SweetAlert2Module.forRoot(),
    HttpClientModule,
    BsDropdownModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgbModule.forRoot()
  ],
  providers: [AlertService, LanguageEventManagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }