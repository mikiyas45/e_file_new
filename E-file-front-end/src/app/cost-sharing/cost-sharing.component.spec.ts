import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostSharingComponent } from './cost-sharing.component';

describe('CostSharingComponent', () => {
  let component: CostSharingComponent;
  let fixture: ComponentFixture<CostSharingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostSharingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostSharingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
