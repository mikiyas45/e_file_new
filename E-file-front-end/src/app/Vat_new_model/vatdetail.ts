export class VatDetail{

    tin: string;
    name: String;
    tax_type: String;
    tax_period: Number;
    tax_year: Number;
    description: String;

    number_of_transaction: Number;
    totalTaxeble: Number;
    totalTaxWithheld: Number;
    tax_details: {
        type: [{
            code: { type: Number, required: false },
            type: { type: String, required: false },
            code_name: { type: String, required: false },
            desc: { type: String, required: false },

            taxableincome: { type: Number, required: false },
            tax_withheld: { type: Number, required: false },

        }]
        // required:true
    }
}
