export class Vat { 
  id : number;
  valueOfTaxableGoodsAndSupplies: Number;
  valueOfZeroRatedGoods :Number;
  vlueOfExemptSupplies :Number;
  valueOfRemittedSupplies :Number;
  outPutVat: Number;
  totalSupplies:Number;
  vatChargeForThisPeriod:Number;
  valueOfLocalPurchasesInput:Number;
  valueOfImportedInputs:Number;
  valueOfGeneralExpenseInputs:Number;
  valueOfPurchasesWithNoVat:Number;
  valueOfTotalInputs:Number;
  valueOfTaxableInputs:Number;
  netVatDueForMonth:Number;
  vatCreditForMonth:Number;
  otherCreditsForMonth:Number;
  creditCarriedForwardFromPreviousMonth:Number;
  amountToBePaid:Number;
  creditAvailableForcarryForward:Number;
  vatOnLocalPurchaseInput:Number;
  vatOnInportedInputs:Number;
  vatOnGeneralExpenseInputs:Number;
  TotalInputTaxCredit:Number;
  VatwithholdingByGovernmentAgencies: Number;
  VATonGovernmentVouchers: Number;
  preparedBy:String;
  checkedBy:String;
  //checkedBy:String;
 // is_active:{type:Boolean;'default':false}
}
