import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithholdingComponent } from './withholding.component';

describe('WithholdingComponent', () => {
  let component: WithholdingComponent;
  let fixture: ComponentFixture<WithholdingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithholdingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithholdingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
