import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VATREGISTRATIONComponent } from './vat-registration.component';

describe('VATREGISTRATIONComponent', () => {
  let component: VATREGISTRATIONComponent;
  let fixture: ComponentFixture<VATREGISTRATIONComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VATREGISTRATIONComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VATREGISTRATIONComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
