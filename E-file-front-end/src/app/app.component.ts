import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {Language } from '../app/Language';
import {LanguageEventManagerService} from './shared/service/language-event-manager.service';
import { config } from './common/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'star-admin-angular';
  public languages = config.LANGUAGES;
  public selectedLanguage: Language = new Language();
  public toggleLanguage = false;
  constructor(private translate: TranslateService,
    private languageEventManagerService: LanguageEventManagerService) {
    translate.setDefaultLang('en');
  }

  switchLanguage(language: { code: string, label: string}) {
    this.selectedLanguage.setCode(language.code);
    this.selectedLanguage.setLabel(language.label);
    this.languageEventManagerService.showLanguage(this.selectedLanguage);
    this.translate.use(language.code.toLowerCase());
}

public toggleLang() {

  this.toggleLanguage ? this.toggleLanguage = false : this.toggleLanguage = true;

}

}
