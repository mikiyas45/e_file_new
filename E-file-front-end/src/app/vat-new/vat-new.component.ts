import { Component, OnInit , ViewChild, ElementRef } from '@angular/core';
import { VatService } from '../vat-new/vat.service';
import { Location } from '@angular/common';
import { VatDetail } from '../Vat_new_model/vatdetail';
import { Vat } from '../vat-new/vat';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Language } from '../Language';
import { config } from '../common/index';
import { code } from '../vat-new/code';
import * as jsPDF from 'jspdf';

//import {Popup} from 'ng2-opd-popup';
//import { AlertService } from './_service'

@Component({
  selector: 'app-vat-new',
  templateUrl: './vat-new.component.html',
  styleUrls: ['./vat-new.component.scss']
})
export class VatNewComponent  {

  @ViewChild('content') content : ElementRef;
  public languages = config.LANGUAGES;
  public selectedLanguage: Language = new Language();
  vat = new Vat();
  vatdetail = new VatDetail();
  msg: string = null;
  public codeType: code = new code();
  //submitted = false;
  vTaxGAndSupplies = 5;
  isDisabled = false;
  constructor(private vatService : VatService , location : Location , private translate: TranslateService) { 
    translate.setDefaultLang('en');
  }
   
  switchLanguage(Language: string){
    this.translate.use(Language);
    console.log('en');
  }

  calculateStuff(){
    return Number(this.vat.valueOfTaxableGoodsAndSupplies) * Number(0.15);
  }
  calculate(){
    return Number(this.vat.valueOfTaxableGoodsAndSupplies) + Number(this.vat.valueOfZeroRatedGoods) + Number(this.vat.vlueOfExemptSupplies) + Number(this.vat.valueOfRemittedSupplies);
  } 
  
  triggerSomeEvent() {
      this.isDisabled = !this.isDisabled;
      this.vat.valueOfTaxableGoodsAndSupplies = 0;
     // this.vat.code = this.codeType.getZero_Rated_Sales();
      this.vat.code = this.codeType.getZero_Rated_Sales();
  
      return;
  }
  public generatePdf(){

    let doc = new jsPDF();
    let specialElementHandler = {
       '#editor' : function (element , renderer) {
         return true;
       }
    };

    let content = this.content.nativeElement;
    doc.fromHTML(content.innerHTML, 15, 15, {
      'width': 190,
      'elementHandlers': specialElementHandler

    } );
    doc.save('confermation.pdf');
  }
  newVat(): void {
   // this.submitted = false;
    this.vat = new Vat();
  }
  getVat(){
    this.vatService.getVat();
  }
 
  addVat() {
   // this.submitted = true;
    this.save();
  }

  private save(): void {
    debugger;
    console.log(this.vat);
  let taxdata={
      code: this.codeType.getValue_Of_Taxable_Goods(),
      code_name:"valueOfTaxableGoodsAndSupplies"  ,
      value: this.vat.valueOfTaxableGoodsAndSupplies,
    };
    // this.vat.tax_detail[0].code_name = "valueOfTaxableGoodsAndSupplies";
    // this.vat.tax_detail[0].code =this.codeType.getValue_Of_Taxable_Goods();

    // this.vat.tax_detail[0].taxableincome = ;
    this.vat.tax_detail.push(taxdata);
    this.vatService.addVat(this.vat)
        .subscribe();
        console.log('Vat Declared Successfully!!');       
       // alert('Vat Declared Successfully!!');
      //  Swal.fire('Vat Declared Successfully!!');
  }
}
