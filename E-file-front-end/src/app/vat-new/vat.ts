export class Vat {
  id: number;
  code: Number;
  valueOfTaxableGoodsAndSupplies:Number;
  tax_detail: Itaxdetail[]=[];

  valueOfZeroRatedGoods: Number;
  vlueOfExemptSupplies: Number;
  valueOfRemittedSupplies: Number;
  outPutVat: Number;
  totalSupplies: Number;
  vatChargeForThisPeriod: Number;
  valueOfLocalPurchasesInput: Number;
  valueOfImportedInputs: Number;
  valueOfGeneralExpenseInputs: Number;
  valueOfPurchasesWithNoVat: Number;
  valueOfTotalInputs: Number;
  valueOfTaxableInputs: Number;
  netVatDueForMonth: Number;
  vatCreditForMonth: Number;
  otherCreditsForMonth: Number;
  creditCarriedForwardFromPreviousMonth: Number;
  amountToBePaid: Number;
  creditAvailableForcarryForward: Number;
  vatOnLocalPurchaseInput: Number;
  vatOnInportedInputs: Number;
  vatOnGeneralExpenseInputs: Number;
  TotalInputTaxCredit: Number;
  VatwithholdingByGovernmentAgencies: Number;
  VATonGovernmentVouchers: Number;
  dateOfFilling: Date;

  preparedBy: String;
  checkedBy: String;
  //checkedBy:String;
  // is_active:{type:Boolean;'default':false}
}
export class tax_detail_class{

  valueOfTaxableGoodsAndSupplies: {
      code: 15;
      code_name:"valueOfTaxableGoodsAndSupplies"  ;
      value: Number;
} 
}
export interface Itaxdetail {
      code: Number;
      code_name:String  ;
      value: Number;
}