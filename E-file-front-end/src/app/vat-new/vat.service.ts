import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vat } from '../vat-new/vat';

/*const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};*/

@Injectable({
  providedIn: 'root'
})
export class VatService {
  
  //private vatUrl = `http://localhost:3000/vat`; 
  private vatUrl = `http://localhost:5013/tax/save`;
  // URL to web api
  constructor(private http: HttpClient) { }

  getVat(): Observable<Vat[]> {

    return this.http.get<Vat[]>(this.vatUrl)
  }

  addVat(vat: Vat): Observable<Vat> {

    return this.http.post<Vat>(this.vatUrl, vat);
  }
}
