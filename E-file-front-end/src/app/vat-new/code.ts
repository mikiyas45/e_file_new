export class code {
  private Value_Of_Taxable_Goods = 5;
  private Zero_Rated_Sales = 15;
  private Tax_Exempt = 20;
  private Remitted_Supplies = 25;
  private Total_Supplies = 30;
  private Local_Purchases = 50;
  private Imported_Inputs = 60;
  private General_Expense_Inputs = 70;
  private Purchases_With_No_Vat = 80;
  private Value_Of_Total_Inputs = 85;
  private Value_Of_Taxable_Inputs = 90;
  private Vat_Due_For_Month = 110;
  private Other_Credits_For_Month = 120;
  private Credits_Carried_Forward = 125;
  private VAT_withholding_by_Government = 130;
  private VAT_on_Government_Vouchers = 140;
  private Amount_To_Be_Paid = 150;
  

  public getValue_Of_Taxable_Goods(): Number {
    return this.Zero_Rated_Sales;
  }
  public setValue_Of_Taxable_Goods(value: number) {
    this.Zero_Rated_Sales = value;
  }

  public getZero_Rated_Sales(): Number {
    return this.Zero_Rated_Sales;
  }
  public setZero_Rated_Sales(value: number) {
    this.Zero_Rated_Sales = value;
  }

  public getTax_Exempt(): Number {
    return this.Zero_Rated_Sales;
  }
  public setTax_Exempt(value: number) {
    this.Zero_Rated_Sales = value;
  }
 
public getRemitted_Supplies() : Number{
  return this.Remitted_Supplies;
}

public setRemitted_Supplies(value: number) {
  this.Remitted_Supplies = value;
}
}
