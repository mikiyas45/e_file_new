import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TablesComponent } from './tables/tables.component';
//import { IconsComponent } from './icons/icons.component';
import { TypographyComponent } from './typography/typography.component';
import { AlertComponent } from './alerts/alerts.component';
import { AccordionsComponent } from './accordions/accordions.component';
import { BadgesComponent } from './badges/badges.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { PaginationComponent } from './pagination/pagination.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { CarouselComponent } from './carousel/carousel.component';
import { TabsComponent } from './tabs/tabs.component';
import { VATREGISTRATIONComponent } from './vat-registration/vat-registration.component';
import { RegisterComponent} from './register/register.component';
import { WithholdingComponent} from './withholding/withholding.component';
import { PensionForPrivateComponent} from './pension-for-private/pension-for-private.component';
import { PensionForCivilComponent} from './pension-for-civil/pension-for-civil.component';
import { CostSharingComponent} from './cost-sharing/cost-sharing.component';
import { VatNewComponent } from './vat-new/vat-new.component';
import { WithholdingNewComponent } from './withholding-new/withholding-new/withholding-new.component';
import { PensionComponent} from './pension/pension.component';
import { EmploymentInTaxDeclarationComponent } from './employment-in-tax-declaration/employment-in-tax-declaration.component';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'forms', component: FormsComponent },
  { path: 'buttons', component: ButtonsComponent },
  { path: 'tables', component: TablesComponent },
 // { path: 'icons', component: IconsComponent },
  { path:'cost-sharing',component:CostSharingComponent},
  { path: 'typography', component: TypographyComponent },
  { path: 'alerts', component: AlertComponent },
  { path: 'accordions', component: AccordionsComponent },
  { path: 'badges', component: BadgesComponent },
  { path: 'progressbar', component: ProgressbarComponent },
  { path: 'breadcrumbs', component: BreadcrumbsComponent },
  { path: 'pagination', component: PaginationComponent },
  { path: 'dropdowns', component: DropdownComponent },
  { path: 'tooltips', component: TooltipsComponent },
  { path: 'carousel', component: CarouselComponent },
  { path: 'vat-registration',component:VATREGISTRATIONComponent},
  { path: 'tabs', component: TabsComponent },
  { path: 'register', component :RegisterComponent},
  { path: 'withholding', component :WithholdingComponent},
  { path:'pension-for-private', component: PensionForPrivateComponent},
  { path: 'pension-for-civil', component: PensionForCivilComponent},
  { path: 'newVat', component: VatNewComponent},
  { path: 'withholding-new', component: WithholdingNewComponent},
  { path: 'pension', component: PensionComponent},
  { path: 'employment-in-tax-declaration', component: EmploymentInTaxDeclarationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
