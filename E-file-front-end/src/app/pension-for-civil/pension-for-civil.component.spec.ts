import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionForCivilComponent } from './pension-for-civil.component';

describe('PensionForCivilComponent', () => {
  let component: PensionForCivilComponent;
  let fixture: ComponentFixture<PensionForCivilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PensionForCivilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionForCivilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
