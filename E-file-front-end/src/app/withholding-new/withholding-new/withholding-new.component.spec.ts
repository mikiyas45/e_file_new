import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithholdingNewComponent } from './withholding-new.component';

describe('WithholdingNewComponent', () => {
  let component: WithholdingNewComponent;
  let fixture: ComponentFixture<WithholdingNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithholdingNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithholdingNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
