import { Component, OnInit } from '@angular/core';
import { Withholding } from './withholding';
import { WithHoldService } from './withhold.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-withholding-new',
  templateUrl: './withholding-new.component.html',
  styleUrls: ['./withholding-new.component.scss']
})
export class WithholdingNewComponent implements OnInit {
  withholding = new Withholding();

 
  constructor(private withholdService : WithHoldService) { }

  ngOnInit() {
  }
  addWithhold() {
    // this.submitted = true;
     this.save();
   }
 
   private save(): void {
     console.log(this.withholding);
     this.withholdService.addWithhold(this.withholding)
         .subscribe();
         console.log('Vat Declared Successfully!!');       
        // alert('Vat Declared Successfully!!');
        // Swal.fire('Withhold Declared Successfully!!');
   }

}
