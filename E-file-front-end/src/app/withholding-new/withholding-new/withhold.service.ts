import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Withholding } from './withholding';

/*const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};*/

@Injectable({
  providedIn: 'root'
})
export class WithHoldService {
  
  //private vatUrl = `http://localhost:3000/vat`; 
  private WithholdingUrl = `http://localhost:5014/withholding/save`;
  // URL to web api
  constructor(private http: HttpClient) { }

  getVat(): Observable<Withholding[]> {

    return this.http.get<Withholding[]>(this.WithholdingUrl)
  }

  addWithhold(vat: Withholding): Observable<Withholding> {

    return this.http.post<Withholding>(this.WithholdingUrl, vat);
  }
}
